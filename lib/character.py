# Extra maths for rounding
import math
# Pseudo random number generator library, by default uses time as seed
import random


class CharacterSheet:
    def __init__(self, name, body, agility, reaction, strength, willpower, logic, intuition, charisma, edge, magic):
        # Attribuites SR5 pg51
        self.name = name
        self.body = body
        self.agility = agility
        self.reaction = reaction
        self.strength = strength
        self.willpower = willpower
        self.logic = logic
        self.intuition = intuition
        self.charisma = charisma
        self.essence = 6
        self.edge = edge
        self.magic = magic
        # Initiative and Condition Monitors: SR5 pg52
        self.initiative = self.reaction + self.intuition
        self.physical_max = math.ceil(self.body / 2) + 8
        self.physical_track = self.physical_max
        self.stun_max = math.ceil(self.willpower / 2) + 8
        self.stun_track = self.stun_max
        self.wound_mod = 0
        self.recovery = self.body + self.willpower + 2  # Quick healer
        # Skills and modifiers
        self.skills = {'summoning': 6, 'spellcasting': 6, 'binding': 6}
        self.foci = {'binding': 2}
        self.pool_mod = 0
        self.spirits = []

    # Roll dice pool, count 1s, 5s, and 6s.
    def roll_pool(self, dice_pool, limit):
        #TODO take in edge boolean and implement rule of six
        raw = []
        results = {'ones': 0, 'fives': 0, 'sixes': 0}
        for i in range(dice_pool):
            roll = random.randint(1, 6)
            raw.append(roll)
            if roll == 1:
                results['ones'] += 1
            elif roll == 5:
                results['fives'] += 1
            elif roll == 6:
                results['sixes'] += 1
            elif roll > 6 or roll < 1:
                print("Dice value out of range")
                raise IndexError
        # If more than half the dice pool rolled were 1s we have a glitch
        glitch = None
        if results['ones'] > (dice_pool / 2):
            glitch = 'glitch'
        # Count 5s and 6s to get hits
        # TODO If edge is used, reroll 6s and count extra hits.
        hits = results['fives'] + results['sixes']
        # Apply limit
        if limit is not None and hits > limit:
            print(f"Rolled {hits} hits but capped at limit of {limit}")
            hits = limit
        # If we have a glitch and no hits we have a critical glitch
        elif hits == 0 and glitch == 'glitch':
            glitch = 'critical'
        return hits, glitch, raw

    def magic_pool(self, skill):
        mod = self.pool_mod + self.wound_mod
        # Check if we have a focus for that skill and if so add it to our poll modifier
        if skill in self.foci:
            mod += self.foci[skill]
        pool = self.magic + self.skills[skill] + mod
        return pool

    def drain_pool(self):
        mod = self.pool_mod + self.wound_mod
        pool = self.willpower + self.intuition + mod
        return pool

    def wound(self, damage, d_type):
        report = ""
        if d_type == 'S':
            self.stun_track -= damage
            if self.stun_track < 1:
                report += f"{self.name} is unconscious.  "
                if self.stun_track < 0:
                    # Convert overflow to physical damage SR5 pg170
                    report += "Remaining damage is now physical. \n"
                    damage = self.stun_track * -1
                    d_type = 'P'
                    # Stun can't go negative
                    self.stun_track = 0
        if d_type == 'P':
            self.physical_track -= damage
            if self.physical_track < -9:
                report += f"{self.name} is dead!\n"
            elif self.physical_track < 1:
                report += f"{self.name} is dying!\n"
        # For each 3 points of damage in the physical and stun tracks, subtract 1 from the pool mod
        damage_total = (self.stun_track - self.stun_max) + (self.physical_track - self.physical_max)
        self.wound_mod = math.ceil(damage_total / 3)
        report += f"Stun monitor = {self.stun_track}/{self.stun_max}, Physical Monitor = {self.physical_track}/" \
                  f"{self.physical_max}, negatives from wounds total {self.wound_mod}"
        print(report)

    def heal(self, damage, d_type):
        if d_type == 'S':
            self.stun_track += damage
            if self.stun_track > self.stun_max:
                self.stun_track = self.stun_max
        elif d_type == 'P':
            self.physical_track += damage
            if self.physical_track > self.physical_max:
                self.physical_track = self.physical_max
        else:
            raise TypeError
        # For each 3 points of damage in the physical and stun tracks, subtract 1 from the pool mod
        damage_total = (self.stun_track - self.stun_max) + (self.physical_track - self.physical_max)
        self.wound_mod = math.ceil(damage_total / 3)
        print(f"Healed {damage}{d_type}. Stun monitor = {self.stun_track}/{self.stun_max}, Physical Monitor = "
              f"{self.physical_track}/{self.physical_max}, negatives from wounds total {self.wound_mod}")

    def rest(self):
        hours = 0
        round_hours = hours
        # Keep making extended tests until all stun is healed
        while self.stun_track < self.stun_max:
            # An extended rest test in hours for stun damage as described SR5 pg207
            pool = self.recovery + self.pool_mod + self.wound_mod
            while pool > 0:
                hits, glitch, _raw = self.roll_pool(pool, None)
                if glitch == 'glitch':
                    print('Gltiched the healing, I aint got time for this!')
                    hours += 2
                elif glitch == 'critical':
                    print('Critical glitch in healing.. ouch!')
                    self.wound(random.randint(1, 3), 'S')
                    hours += 1
                else:
                    hours += 1
                pool -= 1
                self.heal(hits, 'S')
                if self.stun_track == self.stun_max:
                    pool = 0
            print(f"This round of rest took {hours-round_hours}hrs")
            round_hours = hours
        return hours

    def recover(self):
        # An extended rest test in days for physical damage SR5 pg207
        # First recover the stun
        hours = self.rest()
        round_hours = hours
        while self.physical_track < self.physical_max:
            pool = self.recovery + self.pool_mod + self.wound_mod
            # Now recover the physical
            while pool > 0:
                hits, glitch, _raw = self.roll_pool(pool, None)
                if glitch == 'glitch':
                    print('Gltiched the healing, I aint got time for this!')
                    hours += 2 * 24
                elif glitch == 'critical':
                    print('Critical glitch in healing.. ouch!')
                    self.wound(random.randint(1, 3), 'P')
                    hours += 24
                else:
                    hours += 24
                pool -= 1
                self.heal(hits, 'P')
                if self.physical_track == self.physical_max:
                    pool = 0
            print(f"This round of recovery took {(hours-round_hours) / 24} days")
            round_hours = hours
        return hours


# Character sheet for seth
seth = CharacterSheet('Seth', 3, 2, 3, 2, 5, 5, 6, 4, 5, 6)
