# Pseudo random number generator library, by default uses time as seed
import random


class Dice:
    # Roll dice pool, count 1s, 5s, and 6s.
    def roll_pool(self, dice_pool, limit):
        #TODO take in edge boolean and implement rule of six
        raw = []
        results = {'ones': 0, 'fives': 0, 'sixes': 0}
        for i in range(dice_pool):
            roll = random.randint(1, 6)
            raw.append(roll)
            if roll == 1:
                results['ones'] += 1
            elif roll == 5:
                results['fives'] += 1
            elif roll == 6:
                results['sixes'] += 1
            elif roll > 6 or roll < 1:
                print("Dice value out of range")
                raise IndexError
        # If more than half the dice pool rolled were 1s we have a glitch
        glitch = None
        if results['ones'] > (dice_pool / 2):
            glitch = 'glitch'
        # Count 5s and 6s to get hits
        # TODO If edge is used, reroll 6s and count extra hits.
        hits = results['fives'] + results['sixes']
        # Apply limit
        if limit is not None and hits > limit:
            print(f"Rolled {hits} hits but capped at limit of {limit}")
            hits = limit
        # If we have a glitch and no hits we have a critical glitch
        elif hits == 0 and glitch == 'glitch':
            glitch = 'critical'
        return hits, glitch, raw


class Magic(Dice):
    def soak(self, drain, drain_pool):
        hits, _glitch, _raw = self.roll_pool(drain_pool, None)  # No limit on drain resist
        net_drain = drain - hits
        if net_drain < 0:
            net_drain = 0
        return net_drain


class Spell(Magic):
    def __init__(self, char, name, s_type, s_range, duration, d_mod):
        self.caster = char
        self.spell_name = name
        # self.force = 0
        self.spell_type = s_type  # M, P : Mana or Physical
        self.range = s_range  # T, LOS, A : Touch, Line Of Sight, Area
        self.duration = duration  # I,S,P : Instant, Sustained or Permanent
        self.drain_mod = d_mod
        self.drain = 0
        self.drain_type = None  # P, S : Physical or Stun
        # self.hits = 0

    def _get_hits(self, force):
        hits, glitch, raw = self.roll_pool(self.caster.magic_pool('spellcasting'), force)
        print(f"Rolled {raw}")
        print(f"Cast {self.spell_name} at force {force} and rolled {hits} hits")
        if glitch is not None:
            print(f"Rolled a bunch of ones and got a {glitch}")
        # Calculate drain
        if hits > self.caster.magic:
            self.drain_type = 'P'
        else:
            self.drain_type = 'S'
        self.drain = force + self.drain_mod
        if self.drain < 2:
            self.drain = 2
        print(f"Attempting to soak {self.drain}{self.drain_type} drain")
        damage = self.soak(self.drain, self.caster.drain_pool())
        print(f"After the resist roll, {self.caster['name']} takes {damage}{self.drain_type} drain.")
        return hits


class CombatSpell(Spell):
    def __init__(self, direct, damage, element=None, **kwags):
        super(CombatSpell, self).__init__(**kwags)
        self.direct = direct  # True, False
        self.element = element  # None, F, I, E, A : Fire, Ice, Electric, Acid
        self.damage_type = damage  # S, P

    def cast(self, force):
        hits = self._get_hits(force)
        result = ""
        if self.direct:
            result += f"Enemy must now resist {hits}"
            if self.spell_type == 'P':
                result += f" using Body"
            elif self.spell_type == 'M':
                result += f" using Willpower"
            result += f", they then take {hits}{self.damage_type} - their roll in damage, they do not get to soak the" \
                      f" damage"
        else:
            if self.range == 'A':
                # Like a grenade must get at least 3 hits to land on target
                if hits > 2:
                    result += f"all enemies in a {force}m radius take {force + hits}{self.damage_type} damage - their" \
                              f" hits on defense (R+I-2 for area attack.), they resist with body and armour -{force}AP.\n"
                else:
                    # Calculate scatter (2d6 - hits in meters)
                    scatter = random.randint(1, 6) + random.randint(1, 6) - hits
                    d_map = {2: 'back', 3: 'back and left', 4: 'left', 5: 'mostly left and forward',
                             6: 'mostly forward and left', 7: 'forward', 8: 'mostly forward and right',
                             9: 'mostly right and forward', 10: 'right', 11: 'back and right', 12: 'back'}
                    direction = d_map[random.randint(1, 6) + random.randint(1, 6)]
                    result += f'the spell scatters {scatter}m {direction}.  From this point everyone in a {force}m' \
                              f'radius takes {force}{self.damage_type} damage, they resist with body and armour'
            else:
                defense = int(input('What did the defender get on their defense test (Reaction + Intuition)?  Be sure'
                                    ' to account for defense modifiers.\n'))
                hits -= defense
                if hits > 0:
                    result += f"Enemy takes {force + hits}{self.damage_type} with AP of -{force}"
        print(result)


class DetectionSpell(Spell):
    def __init__(self, active, **kwags):
        super(DetectionSpell, self).__init__(**kwags)
        self.active = active  # True, False

    def cast(self, force):
        hits = self._get_hits(force)
        result = f"The range is {force * self.caster.magic}m.\n"
        if self.spell_name == 'Mindnet':
            result += f'Your mind is joined with up to {force} others.'
        elif self.active:
            result += f'Target(s) resist your {hits} hits with Willpower + Logic [Mental] or Force x 2 for magical ' \
                      f'objects.\n'
            result += "With the net hits, you know:\n" \
                      '1. Only general knowledge, no details.\n    Detect Life example: A group of metahumans.\n'\
                      '2. Major details only, no minor details. \n    Detect Life example: A dwarf, a troll, and an ' \
                      'ork walk into a bar.\n'\
                      '3. Major and minor details, with some minor details obscured or missing.\n    Detect Life ' \
                      'example: A dwarf, a troll, and an ork walk into a bar. They are all armed, and their ' \
                      'weapons are out. The troll is leading.\n'\
                      '>3. Detailed information. \n    Detect Life example: A dwarf, a troll, and an ork walk into a ' \
                      'bar. They are all armed, and their weapons are out. The troll leading is your contact, ' \
                      'Moira; you don’t believe you’ve ever met the other two.'
        print(result)


class HealthSpell(Spell):
    def __init__(self, essence=6, **kwags):
        super(HealthSpell, self).__init__(**kwags)
        self.essence = essence

    def cast(self, force):
        essence_mod = self.essence - 6
        og_mod_pool = self.caster.pool_mod
        self.caster.pool_mod += essence_mod
        hits = self._get_hits(force)
        self.caster.pool_mod = og_mod_pool
        result = ""
        if self.spell_name == 'Heal':
            result += f'Target heals {hits} boxes of physical damage'
        elif self.spell_name == 'Increase Reflexes':
            i_die = int(hits / 2)
            result += f'Add +{hits}+{i_die}D6 to targets initiative'
        print(result)


class ManipulationSpell(Spell):
    def __init__(self, m_type, **kwags):
        super(ManipulationSpell, self).__init__(**kwags)
        self.m_type = m_type  # D, M, E, P : Damaging, Mental, Environmental, Physical

    def cast(self, force):
        hits = self._get_hits(force)
        if self.spell_name == 'Shapechange':
            print(f"You transform into an owl with an Agility of {5+hits}")
        elif self.spell_name == 'Physical Barrier':
            print(f"A shimmering force field with Structure Rating {hits} and Armor {hits} appears. It can be a dome "
                  f"with radius of {force}m or a wall of height {force}m and length {force*2}m.")
                # TODO include details about shooting through or destroying barriers SR5 pg198
        elif self.spell_name == 'Levitate':
            print(f"You can lift up to {hits*200}kg and move it up to {force}m per Combat Turn.  Unwilling subject can "
                  f"resist with Body + Strength vs the {hits} hits")
        if self.m_type == 'M':
            print(f"Target resists {hits} hits with Logic + Willpower.  Each round target may make another test with a "
                  f"complex action to reduce net hits.")
            if self.spell_name == 'Influence':
                print("For influence they only make this test if confronted with the 'wrongness' of the suggestion.")
        # result = ""


class IllusionSpell(Spell):
    def __init__(self, realistic, senses, **kwags):
        super(IllusionSpell, self).__init__(**kwags)
        self.realistic = realistic  # True, False
        self.senses = senses  # Multi sense or Single sense

    def cast(self, force):
        hits = self._get_hits(force)
        result = f"The {hits} hits are resisted with "
        if self.spell_type == 'M':
            result += "Logic + Willpower"
        elif self.spell_type == 'P':
            result += "Intuition + Logic (or Object Resistance)"
        if self.spell_name == 'Bugs':
            result += ', for each net hit, reduce the targets initiative by 2.'
        print(result)


class Conjuring(Magic):
    def __init__(self, char, name):
        self.caster = char
        self.spirit_name = name
        self.bound = False
        self.force = None
        self.services = 0
        self.drain_type = None

    def summon(self, force):
        # Summoning a spirit as defined SR5 pg 300
        print(f"Attempting to summon a force {force} {self.spirit_name} spirit")
        # Roll summoning test for caster
        hits, glitch, raw = self.roll_pool(self.caster.magic_pool('summoning'), force)
        print(f"{self.caster.name} rolled: {raw}")
        if glitch == 'glitch':
            print(f"{self.caster.name} glitches on his summoning test")
        elif glitch == 'critical':
            print(f"Oh drek! {self.caster.name} got a critical glitch!")

        # Roll the dice pool equal to force of Spirit to resist the summoning
        s_hits, s_glitch, s_raw = self.roll_pool(force, None)
        print(f"Spirit rolled: {s_raw}")
        if s_glitch == 'glitch':
            print("Spirit got a glitch")
        elif s_glitch == 'critical':
            print("Spirit got a critical glitch")

        # Calculate summoning result
        self.services = hits - s_hits
        if self.services < 1:
            print("Summoning failed")
        else:
            print(f"{self.caster.name} has summoned a force {force} spirit owing {self.services} services")
            self.force = force

        # Soak drain
        if force > self.caster.magic:
            self.drain_type = 'P'
        else:
            self.drain_type = 'S'
        damage = self.soak(s_hits * 2, self.caster.drain_pool())
        print(f"After soaking, {self.caster.name} takes {damage}{self.drain_type} drain.")
        self.caster.wound(damage, self.drain_type)

        # Return true if spirit was successfully summoned
        if self.services > 0:
            return True
        else:
            return False

    def bind(self):
        # Bind the spirit for extra services and so it doesn't vanish at sunrise. SR5 pg300
        b_hits, b_glitch, b_raw = self.roll_pool(self.caster.magic_pool('binding'), None)
        s_hits, _s_glitch, s_raw = self.roll_pool(self.force * 2, None)
        net_hits = b_hits - s_hits
        # spirit_bound = None
        if net_hits > 0:
            extra_services = net_hits - 1
            self.services += extra_services
            print(f'Success! The spirit is now bound with an extra {extra_services} for a total of '
                  f'{self.services} services!')
            self.bound = True
        else:
            print(f"Failure! The spirit could not be bound.")
            print(f"Caster: {b_raw}\nVS\nSpirit: {s_raw}")
            self.bound = False
        # Soak the drain
        drain = s_hits * 2
        print(f"Attempting to soak {drain}{self.drain_type} drain")
        bind_damage = self.soak(drain, self.caster.drain_pool())
        print(f"After the resist roll, {self.caster.name} takes {bind_damage}{self.drain_type} drain.")
        self.caster.wound(bind_damage, self.drain_type)
