# Simple script to calculate the services and drain from summoning a spirit or casting a spell in Shadowrun 5e

# Import argument parser for force of spirit or spell (and eventually edge)
import argparse
# Use spell classes from local library
import lib.magic as magic
import lib.character as character

# Initialise the argument parser
parser = argparse.ArgumentParser(
                prog='ShadowRoller',
                description='Calculate services and drain when summoning a spirit in Shadowrun 5e'
                )
parser.add_argument('-f', '--force', type=int, help='The force of the spirit or spell')
parser.add_argument('-s', '--spell', type=str, help='The spell to cast')
parser.add_argument('-b', '--bind', action='store_true', help='Go into spirit binding mode')
parser.add_argument('-t', '--test', action='store_true', help='Go into testing mode')
parser.add_argument('-m', '--modifier', type=int, help='Dice pool modifier, can be positive or negative')



# Take in Force of spirit
args = parser.parse_args()
FORCE = args.force
# Take in spell
SPELL = args.spell

seth = character.seth

if args.modifier:
    seth.pool_mod += args.modifier
    print(f"Taken in modifier of {args.modifier}")


spellbook = {'ball_light': magic.CombatSpell(False, 'P', element='E', char=seth, name='Ball Lightning', s_type='P',
                                             s_range='A', duration='I', d_mod=-1),
             'flame': magic.CombatSpell(False, 'P', element='F', char=seth, name='Flamethrower', s_type='P',
                                        s_range='LOS', duration='I', d_mod=-3),
             'heal': magic.HealthSpell(char=seth, name='Heal', s_type='M', s_range='T', duration='P',
                                       d_mod=-4),
             'reflex': magic.HealthSpell(char=seth, name='Increase Reflexes', s_type='P', s_range='T', duration='S',
                                         d_mod=0),
             'shape': magic.ManipulationSpell('P', char=seth, name='Shapechange', s_type='P', s_range='T', duration='S',
                                              d_mod=-3),
             'influence': magic.ManipulationSpell('M', char=seth, name='Influence', s_type='M', s_range='LOS',
                                                  duration='P', d_mod=-1),
             'levitate': magic.ManipulationSpell('P', char=seth, name='Levitate', s_type='P', s_range='LOS',
                                                 duration='S', d_mod=-2),
             'barrier': magic.ManipulationSpell('E', char=seth, name='Physical Barrier', s_type='P', s_range='A',
                                                duration='S', d_mod=-1),
             'bugs': magic.IllusionSpell(True, 'multi', char=seth, name='Bugs', s_type='M', s_range='LOS', duration='S',
                                         d_mod=-3),
             'invisible': magic.IllusionSpell(True, 'multi', char=seth, name="Improved Invisibility", s_type='P',
                                              s_range='T', duration='S', d_mod=-1),
             'phantasm': magic.IllusionSpell(True, 'multi', char=seth, name='Trid Phantasm', s_type='P', s_range='A',
                                             duration='S', d_mod=0),
             'replay': magic.DetectionSpell(True, char=seth, name="Death Replay", s_type='M', s_range='T', duration='S',
                                            d_mod=-3),
             'detect_life': magic.DetectionSpell(True, char=seth, name="Detect Life", s_type='M', s_range='A',
                                                 duration='S', d_mod=-3),
             'mindnet': magic.DetectionSpell(True, char=seth, name='Mindnet', s_type='M', s_range='T', duration='S',
                                             d_mod=0),
             'thoughts': magic.DetectionSpell(True, char=seth, name='Thought Recognition', s_type='M', s_range='LOS',
                                              duration='S', d_mod=0)}

if args.test:
    seth.wound(24, "S")
    seth.recover()
# Check if we're in binding mode
elif args.bind:
    binding = True
    f = None
    hours_taken = 0
    spirit_types = ['Air', 'Earth', 'Fire', 'Water', 'Guide']
    while binding:
        choice = input("What type of spirit do you want to summon? (1)Air, (2)Earth, (3)Fire, (4)Water, (5)Guide "
                       "or (r) for rest, (q) for Quit) ")
        if choice == 'q':
            binding = False
        elif choice == 'r':
            hours_taken += seth.recover()
        else:
            if FORCE is None:
                f = int(input("Enter the desired spirit force:  "))
            else:
                f = FORCE
            spirit = magic.Conjuring(char=seth, name=spirit_types[int(choice) - 1])
            success = spirit.summon(f)
            spirits_bound = 0
            for s in seth.spirits:
                if s.bound:
                    spirits_bound += 1
            if success and spirits_bound < seth.charisma:
                choice = input("Do you want to try and bind the spirit? (y/N)\n")
                if choice == 'y':
                    hours_since_summoned = hours_taken
                    while not spirit.bound:
                        spirit.bind()
                        hours_taken += f
                        if seth.physical_track < 1:
                            print(f"{seth.name} is bleeding, he needs a holiday.")
                            hours = seth.recover()
                            print(f"Recovery took {hours/24} days.")
                            hours_taken += hours
                        elif seth.stun_track < 1:
                            print(f"{seth.name} is unconscious, sleepy time.")
                            hours = seth.rest()
                            print(f"Slept for {hours}hrs")
                            hours_taken += hours
                        if not spirit.bound:
                            hours_since_summoned = hours_taken - hours_since_summoned
                            if hours_since_summoned > 12:
                                print(f"It has been {hours_since_summoned}hrs since the spirit was summoned, times up.")
                                success = False
                                break
                            else:
                                choice = input(f"Do you want to try binding again? (Y/n/r)")
                                if choice == 'n':
                                    break
                                elif choice == 'r':
                                    seth.recover()
            if success:
                seth.spirits.append(spirit)
            if seth.stun_track < 1 or seth.physical_track < 1 or len(seth.spirits) > 4:
                binding = False
    print("Seth now commands:")
    for s in seth.spirits:
        print(f"Force {s.force} {s.spirit_name} spirit with {s.services} services. Bound = {s.bound}")
    print(f"Time taken = {hours_taken}hrs or {hours_taken / 24} days")
else:
    # If no force was specified as an argument, ask for one.
    if FORCE is None:
        FORCE = int(input("Enter the desired spell force:  "))
    # If no spell was specified, ask for one.
    if SPELL is None:
        print(spellbook.keys())
        SPELL = input('Choose spell from above:')
    spellbook[SPELL].cast(FORCE)
