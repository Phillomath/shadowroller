# ShadowRoller

## A basic script to help me remember all the steps for spirit summoning and spellcasting in Shadowrun 5th Edition
This requires python3.  Once that is installed you can run this in a terminal with the following command:
```commandline
python3 main.py
```
You can add command line arguments to specify the force or spell like so:
```
python3 main.py -f 6 -s ball_light
```
To run a binding session to try and bind a collection of spirits:
```commandline
python3 main.py -b
```